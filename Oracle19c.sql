--
-- ER/Studio Data Architect SQL Code Generation
-- Project :      library database.dm1
--
-- Date Created : Thursday, June 13, 2024 21:03:40
-- Target DBMS : Oracle 19c
--

-- 
-- TABLE: address 
--

CREATE TABLE address(
    "address id"  CHAR(10)          NOT NULL,
    street        VARCHAR2(4000),
    city          VARCHAR2(4000),
    state         VARCHAR2(4000),
    zip           VARCHAR2(4000),
    PRIMARY KEY ("address id")
)
;



-- 
-- TABLE: book 
--

CREATE TABLE book(
    title             VARCHAR2(300)     NOT NULL,
    author            VARCHAR2(200)     NOT NULL,
    available         NUMBER(1, 0),
    language          VARCHAR2(4000),
    pages             NUMBER(38, 0),
    published_date    DATE,
    PRIMARY KEY (title, author)
)
;



-- 
-- TABLE: chapter 
--

CREATE TABLE chapter(
    "Chapter Number"  CHAR(10)         NOT NULL,
    heading           CHAR(100)        NOT NULL,
    "page count"      NUMBER(38, 0),
    title             VARCHAR2(300)    NOT NULL,
    author            VARCHAR2(200)    NOT NULL,
    PRIMARY KEY ("Chapter Number", title, author)
)
;



-- 
-- TABLE: checkout 
--

CREATE TABLE checkout(
    date              DATE,
    title             VARCHAR2(300)    NOT NULL,
    author            VARCHAR2(200)    NOT NULL,
    duration          NUMBER(38, 0),
    patron_number     CHAR(10)         NOT NULL,
    library_number    CHAR(10)         NOT NULL,
    PRIMARY KEY (title, author, patron_number)
)
;



-- 
-- TABLE: library 
--

CREATE TABLE library(
    library_number    CHAR(10)         NOT NULL,
    "library name"    VARCHAR2(300),
    "address id"      CHAR(10)         NOT NULL,
    PRIMARY KEY (library_number)
)
;



-- 
-- TABLE: patron 
--

CREATE TABLE patron(
    patron_number    CHAR(10)          NOT NULL,
    name             VARCHAR2(4000),
    PRIMARY KEY (patron_number)
)
;



-- 
-- TABLE: publisher 
--

CREATE TABLE publisher(
    name          VARCHAR2(4000)    NOT NULL,
    founded       DATE,
    location      VARCHAR2(4000),
    "address id"  CHAR(10)          NOT NULL,
    PRIMARY KEY (name)
)
;



-- 
-- TABLE: REPCAT$_REPCATLOG 
--

CREATE TABLE REPCAT$_REPCATLOG(
    VERSION         NUMBER,
    ID              NUMBER            NOT NULL,
    SOURCE          VARCHAR2(128)     NOT NULL,
    USERID          VARCHAR2(128),
    TIMESTAMP       DATE,
    ROLE            VARCHAR2(1)       NOT NULL,
    MASTER          VARCHAR2(128)     NOT NULL,
    SNAME           VARCHAR2(128),
    REQUEST         NUMBER            
                    CONSTRAINT REPCAT$_REPCATLOG_REQUEST CHECK (request IN (-1, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10,
                                       11, 12, 13, 14, 15, 16, 17,
                                       18, 19, 20, 21, 22, 23, 24, 25)),
    ONAME           VARCHAR2(128),
    TYPE            NUMBER            
                    CONSTRAINT REPCAT$_REPCATLOG_TYPE CHECK (type IN (-1, 0, 1, 2, 4, 5, 7, 8, 9, 11, 12, -3,
                                    13, 14, 32, 33)),
    A_COMMENT       VARCHAR2(2000),
    BOOL_ARG        VARCHAR2(1),
    ANO_BOOL_ARG    VARCHAR2(1),
    INT_ARG         NUMBER,
    ANO_INT_ARG     NUMBER,
    LINES           NUMBER,
    STATUS          NUMBER            
                    CONSTRAINT REPCAT$_REPCATLOG_STATUS CHECK (status IN (0, 1, 2, 3, 4)),
    MESSAGE         VARCHAR2(200),
    ERRNUM          NUMBER,
    GNAME           VARCHAR2(30),
    PRIMARY KEY (ID, SOURCE, ROLE, MASTER)
)
STORAGE(FREELISTS 1
        )
;



-- 
-- TABLE: section 
--

CREATE TABLE section(
    section_number          CHAR(10)         NOT NULL,
    "number of paragraphs"  NUMBER(38, 0),
    "word count"            NUMBER(38, 0),
    "Chapter Number"        CHAR(10)         NOT NULL,
    title                   VARCHAR2(300)    NOT NULL,
    author                  VARCHAR2(200)    NOT NULL,
    PRIMARY KEY (section_number, "Chapter Number", title, author)
)
;



-- 
-- TABLE: TEST 
--

CREATE TABLE TEST(
    ID      NUMBER,
    NAME    VARCHAR2(255)
)
;



